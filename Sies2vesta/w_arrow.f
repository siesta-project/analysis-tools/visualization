C...............................................................
C
      subroutine write_arrow(io1,is1,nbox,ivmin,ivmax,iev,
     .                       cc_ang,nat,iz,freq,disp,label,
     .                      syslab,color,fact,diam,pen)   
C     syslab pour le bon format d'ecriture                                                    
C     write down atom coordinates and displacement vectors
C     in the format for Xcrysden and VESTA
      implicit none
      integer io1,is1,nat,nbox,ibox,iev,ivmin,ivmax,iat,ii,jj,
     .        ii1,iz(nat),color(3),pen
      character outfil*60,syslab*30,inpfil*60,suffix*6
      double precision cc_ang(3,3),coort(3),coordr(3), 
     .                 cc_angi(3,3), 
     .       disp(3,nat,ivmin:ivmax),freq(ivmin:ivmax),
     .       btoang,dscal,twopi,aa,bb,cc,alpha,beta,jamma,
     .       occ,long,fact,diam
      character(len=2) label(nat)
      double precision, allocatable :: mass(:),coord(:,:)   
      data btoang/0.529177/   !  Bohr to Angstroem
      data dscal /0.10/       !  A convenient scale factor for arrows' length
      external inver3  
      write(io1,202)
  202 format ('#VESTA_FORMAT_VERSION 3.0.0')            
      write (io1,211) iev,freq(iev)
      rewind is1
C --- header as for molecule (= selected atoms in the box):
C --  extract crystal cell parameters
C     taken by default vesta (pris par defaut par vesta)
C      aa = 1.0000000     ! Angstroem
C      bb = 1.0000000
C      cc = 1.0000000
C      alpha = 90.0000000  ! degree
C      beta  = 90.0000000
C      jamma = 90.0000000
      inpfil = trim(syslab)//'.XV'
      write (6,*) 'Found and opened: ',inpfil
      write (io1,*) '# Molecule structure from ',trim(syslab),'.XV'
      write (io1,*) trim(syslab),'.XV'
      write (io1,*) 'MOLECULE'
C      write (io1,*) '# Lattice parameters:'             
C      write (io1,*) 'CELLP'
C      write  (io1,230) aa,bb,cc,alpha,beta,jamma
C  230 format(6f12.7,/, 6('   0.0000000'))                           
C      call inver3(cc_ang,cc_angi)      ! not necessary
      write (io1,*) '# Atom coordinates in Angstroem:'
      write (io1,*) 'STRUC'
      occ=1.0000
C      call inver3(cc_ang,cc_angi)

      write (6,*) 'nbox=',nbox
      do ibox=1,nbox
      read  (is1,'(i4,3f20.8)') iat, (coort(jj),jj=1,3)
 
      write (io1,201) ibox,label(iat),label(iat), 
     .              occ,(coort(jj),jj=1,3)
  201 format(i5,2a6,f8.4, 3f14.7,/25x, 3('     0.0000000'))
      enddo
      write (io1,*) '  0 0 0 0 0 0 0 0 0'
      write (io1,203) 
  203 format('ARROW')
      rewind is1   ! read coordinates of atoms in the box from  is1
      do ibox=1,nbox
      read  (is1,'(i4,3f20.8)') iat
        long = sqrt(disp(1,iat,iev)**2+disp(2,iat,iev)**2+
     .         disp(3,iat,iev)**2)
   
      write (io1,'(i4,3f12.7,2x,2f9.3,2x,4i4,/,2i4)') ibox,     
     .              (disp(jj,iat,iev)*dscal,jj=1,3),      
     .              long*fact,diam,color,pen,                   
     .              ibox-1,-1                                
      enddo
C    ibox i4;color 3i4,pen i4
C    -1 Otherwise lack of arrow (sinon manque de fleches)
C     diameter=0.30,color:100,27,60
C     1:penetrating arrow,or 0: not penetrating arrow
C     ibox-1: index by identifying struc (indice repérer par struc)
C     -1:always ends with -1 (se termine toujours par -1)
      write (io1,220)
  220 format('0 0 0 0 0 0 0 0 0')
      return

  211 format ('# ---- XSF block for ---- iev =',i6,
     .        '  freq = ',f14.6,' cm-1')
      end
